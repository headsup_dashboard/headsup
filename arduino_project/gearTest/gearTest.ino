/**
*       1
*     =====
* 2  / 3  / 0
*    =====
* 7 /    / 5
*   =====    * 4
*   6   
*/

#define CLK 7 //siva
#define LATCH_SEG 9 //zelena
#define DATA_SEG 8 //bijela
#define LATCH_RPM 5 //ljubicasta
#define DATA_RPM 6 //zuta

int GEAR[] = {B11100111,
              B10000100,
              B11010011,
              B11010110,
              B10110100,
              B01110110,
              B01110111,
              B11000100,
              B00000000, 
              B11100101 };

void setup(){
  pinMode(CLK, OUTPUT);
  pinMode(LATCH_SEG, OUTPUT);
  pinMode(DATA_SEG, OUTPUT);
  pinMode(LATCH_RPM, OUTPUT);
  pinMode(DATA_RPM, OUTPUT);
  pinMode(18, OUTPUT);
  pinMode(19, OUTPUT);
  pinMode(4, OUTPUT);
  pinMode(20, OUTPUT);
  pushGear(8);
  pushRPM(0);
  digitalWrite(18, HIGH);
  digitalWrite(19, HIGH);
  digitalWrite(4, LOW);
  Serial1.begin(115200);
}

void loop(){
  testComm();
  //testLeds();
  //if (Serial.available())
  //  shitUp();
}

void shitUp() {
  int i;
  digitalWrite(4, HIGH);
  for (i=0; i<9; i++) {
    singleRPMled(i);
    delay(50);
  }
  pushRPM(8);
  pushGear(9);
  digitalWrite(2, LOW);
  digitalWrite(3, LOW);
  delay(200);
  pushGear(8);
  digitalWrite(2, HIGH);
  digitalWrite(3, HIGH);
  delay(50);
  pushRPM(0);
  pushGear(8);
  digitalWrite(4, LOW);
}

void testComm() {
  char inChar;
  String inString = "";
  
  while (Serial1.available()) {
    inChar = Serial1.read();
    inString += inChar;
    Serial1.println(inString);
  };
  
    pushRPM(inString[1]-'0');
    pushGear(inString[3]-'0');
    setWater(inString[5]-'0');
    setOil(inString[7]-'0');
    setShift(inString[9]-'0');
}

void testLeds() {
  digitalWrite(18, LOW);
  delay(500);
  digitalWrite(18, HIGH);
  digitalWrite(4, HIGH);
  delay(500);
  digitalWrite(4, LOW);
  digitalWrite(19, LOW);
  delay(500);
  digitalWrite(19, HIGH);
  Serial.println("bok");
  for (int i=0; i<=7; i++){
    pushGear(i);
    pushRPM(i+1);
    delay(500);
  }
  pushRPM(0);
  pushGear(8);
}

void pushGear(int gear){
  if (gear<0 || gear>8)
    return;
  digitalWrite(DATA_SEG, 0);
  for (int i=0; i<7; i++)
    tick(CLK);
  for (int i=0; i<=7; i++) {
    int bitmask = 1 << i;
    digitalWrite(DATA_SEG, GEAR[gear] & bitmask);
    tick(CLK);
  }
  tick(LATCH_SEG);
}

void pushRPM(int val){
  if (val<0 || val>8)
    return;
  clearRPMreg();
  for (int i=8; i>=val+1; i--) {
    digitalWrite(DATA_RPM, 0);
    tick(CLK); }
  for (int i=0; i<val; i++) {
    digitalWrite(DATA_RPM, 1);
    tick(CLK); }
  for (int i=val; i<8; i++) {
    digitalWrite(DATA_RPM, 0);
    tick(CLK); }
  tick(LATCH_RPM);
}

void setWater(int val) {
  if (val==1)
    digitalWrite(18, LOW);
  if (val==0)
    digitalWrite(18, HIGH);
}

void setOil(int val) {
  if (val==1)
    digitalWrite(19, LOW);
  if (val==0)
    digitalWrite(19, HIGH);
}

void setShift(int val) {
  if (val==0)
    digitalWrite(4, LOW);
  if (val==1)
    digitalWrite(4, HIGH);
}

void singleRPMled(int n) {
  int i;
  clearRPMreg();
  for (i=0; i<n; i++) {
    digitalWrite(DATA_RPM, 0);
    tick(CLK); }
  digitalWrite(DATA_RPM, 1);
  tick(CLK);
  for (i=n+1; i<9; i++) {
    digitalWrite(DATA_RPM, 0);
    tick(CLK); }
  tick(LATCH_RPM);
}

void clearRPMreg () {
  digitalWrite(DATA_RPM, 0);
  for (int i=0; i<=7; i++)
    tick(CLK);
}

void tick(int pin){
  digitalWrite(pin, 0);
  digitalWrite(pin, 1);
}
