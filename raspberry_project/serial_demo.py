from serial import Serial
from time import sleep
from random import randint
from twisted.internet.protocol import DatagramProtocol
from twisted.internet import reactor

_port = "/dev/ttyAMA0"
_baud = 115200

class DriverPanel:
	comPort = Serial
	
	def __init__(self, port, baud):
		self.comPort = Serial(port, baud)
		self.comPort.close()
		self.comPort.open()
	
	def setGear(self, gear):
		a = "G" + str(gear) + '\r\n' 
		self.comPort.write(a);

	def setRPM(self, rpm):
		a = "R" + str(rpm) + '\r\n' 
		self.comPort.write(a);

	def setWater(self, led):
		a = "W" + str(led) + '\r\n' 
		self.comPort.write(a);

	def setOil(self, led):
		a = "O" + str(led) + '\r\n'
                self.comPort.write(a);
	def setShift(self, led):
		a = "S" + str(led) + '\r\n'
                self.comPort.write(a);

	def comClose(self):
		self.comPort.close()


def demoRun():
	rpm = 0
	gear = 0
	oldRPM = 0
	increasing = True
	water = False
	oil = False
	shift = False
	while True:
		oldRPM = rpm
		if increasing:
			delta = randint(0,1)
			sleep_time = randint(50,100) / 1000.0
		else:
			delta = -randint(0,1)
			sleep_time = randint(100,250) / 1000.0
		
		if randint(0,30) == 0:
			water = not water
			driver.setWater(int(water))
		if randint(0,30) == 0:
			oil = not oil
			driver.setOil(int(oil))
		if randint(0,30) == 0:
			shift = not shift
			driver.setShift(int(shift))

		
		rpm = rpm + delta

		if rpm > 8:
			rpm = 2
			gear = gear + 1
			driver.setGear(gear)
		if rpm < 0:
			rpm = 8
			gear = gear - 1
			driver.setGear(gear)

		if gear >= 7:
			increasing = False
		if gear <= 0:
			increasing = True

		print increasing, delta, rpm, gear
		if oldRPM != rpm:
			driver.setRPM(rpm)
		sleep(sleep_time)

class EchoUDP(DatagramProtocol):
    def datagramReceived(self, datagram, address):
        self.transport.write(datagram, address)


reactor.listenUDP(8000, EchoUDP())
reactor.run()
driver = DriverPanel(_port, _baud)
driver.setGear(1)
driver.setRPM(1)
demoRun()
driver.comClose()