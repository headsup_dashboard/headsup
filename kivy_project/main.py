#!/usr/bin/python2.7
# import kivy
#kivy.require('1.7.0')

from kivy.app import App
from kivy.uix.image import Image, AsyncImage
from kivy.lang import Builder
from kivy.properties import NumericProperty
from kivy.config import Config

from math import sin, cos, pi

from twisted.internet.protocol import DatagramProtocol
from twisted.internet import reactor

from threading import Thread


class ClientDatagramProtocol(DatagramProtocol):

    def __init__(self, app):
        self.app = app

    def startProtocol(self):
        self.transport.connect(rpi_ip_addr, send_port)
        self.register()

    def register(self):
        datagram = 'register'

        self.transport.write(datagram)

    def datagramReceived(self, datagram, host):
        print 'Datagram received: ', repr(datagram)
        datagram = datagram.split('|')

        self.app.display_rpm(datagram[0])
        self.app.display_gear(datagram[1])
        self.app.set_engine_check_light(datagram[2])
        self.app.set_oil_check_light(datagram[3])


# ip_addr = get_lan_ip()
ip_addr = '127.0.0.1'
rpi_ip_addr = '192.168.0.1'

listen_port = 8000
send_port = 8000


Builder.load_string('''
<RotatedImage>:
    canvas.before:
        PushMatrix
        Rotate:
            angle: root.angle
            axis: 0, 0, 1
            origin: root.center
    canvas.after:
        PopMatrix
''')


class RotatedImage(Image):
    angle = NumericProperty()


class HeadsUpApp(App):
    title = 'Heads Up'
    icon = 'logo.png'
    kv_directory = 'templates'

    def build(self):
        self.engine_check_light = False
        self.oil_check_light = False
        self.current_gear = 0
        self.speed_indicators = 0
        self.speed_indicator_texture = Image(
            source='static/img/speed_point.png').texture

    def display_gear(self, gear, *args):
        if self.root:
            self.current_gear = int(gear)
            self.root.ids.gear_label.text = '{}'.format(self.current_gear)

    def display_rpm(self, rpm, *args):
        if self.root:
            current_rpm = int(rpm)

            current_speed = (
                (self.current_gear) * 20 + 20 / 14000.0 * (current_rpm - 9000)
            )

            self.root.ids.speed_label.text = '{} km/h'.format(
                int(current_speed))

            if current_rpm > 11000:
                self.root.ids.gear_label.text = (
                    '[color=ff3333]{}[/color]'.format(
                        self.root.ids.gear_label.text)
                )
                self.root.ids.gear_label.markup = True

            else:
                self.root.ids.gear_label.text = (
                    '{}'.format(self.current_gear)
                )
                self.root.ids.gear_label.markup = True

            # build speed indicators
            radius = 207
            max_display_angle = 114
            starting_display_angle = 102

            speed_layout = self.root.ids.speed_layout

            center_x = speed_layout.x
            center_y = speed_layout.y - 148

            display_angle_range = range(
                starting_display_angle - int(
                    current_rpm / 15000.0 * max_display_angle
                ),
                starting_display_angle
            )

            current_speed_indicators = 0

            for i in reversed(display_angle_range):

                current_speed_indicators += 1

                if self.speed_indicators >= current_speed_indicators:
                    continue

                current_angle = i * 2
                radians = current_angle * pi / 180
                x = center_x + radius * cos(radians)
                y = center_y + radius * sin(radians)

                speed_layout.add_widget(RotatedImage(
                    texture=self.speed_indicator_texture,
                    pos=(x, y),
                    angle=current_angle
                ))

            if current_speed_indicators < self.speed_indicators:

                child_delta = self.speed_indicators - current_speed_indicators

                for child in speed_layout.children[:]:
                    if isinstance(child, RotatedImage):
                        self.speed_indicators += 1
                        speed_layout.remove_widget(child)
                        child_delta -= 1

                    if child_delta == 0:
                        break

            self.speed_indicators = current_speed_indicators

    def set_oil_check_light(self, value, *args):

        if self.oil_check_light == int(value):
            return

        if self.oil_check_light:
            self.root.ids.oil_img.source = (
                'static/img/off/oil_check_light.png'
            )
            self.oil_check_light = False
        else:
            self.root.ids.oil_img.source = (
                'static/img/on/oil_check_light.png'
            )
            self.oil_check_light = True

    def set_engine_check_light(self, value, *args):

        if self.engine_check_light == int(value):
            return

        if self.engine_check_light:
            self.root.ids.engine_img.source = (
                'static/img/off/engine_check_light.png'
            )
            self.engine_check_light = False
        else:
            self.root.ids.engine_img.source = (
                'static/img/on/engine_check_light.png'
            )
            self.engine_check_light = True

    def on_stop(self):
        print 'STOPPING!'
        reactor.stop()

if __name__ == '__main__':

    Config.set('graphics', 'height', 600)
    Config.set('graphics', 'width', 840)
    Config.set('graphics', 'resizable', 0)
    Config.write()
    app = HeadsUpApp()

    # twisted protocol
    protocol = ClientDatagramProtocol(app)
    reactor.listenUDP(listen_port, protocol)
    t = Thread(target=reactor.run, args=(False,))
    t.start()

    app.run()

    # reactor.run()
