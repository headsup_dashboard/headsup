from random import randint
from time import sleep

from twisted.internet.protocol import DatagramProtocol
from twisted.internet import reactor


# Here's a UDP version of the simplest possible protocol
class Server(DatagramProtocol):

    def datagramReceived(self, datagram, address):
        print address
        #self.transport.connect(address[0], 8000)
            #self.transport.write("Hello world")

        self.serve(address[0])

    def serve(self, address):
        self.transport.connect(address, 8000)
        self.simulate()

    def send_datagram(self, rpm, gear, check_engine, check_oil):
        self.transport.write('{}|{}|{}|{}'.format(
            str(rpm), str(gear), str(check_engine), str(check_oil))
        )

    def simulate(self):

        gear = 1
        rpm = 1000
        check_engine = 0
        check_oil = 0
        increasing = True

        rpm_min_treshold = 1000
        rpm_max_treshold = 13000

        while True:
            delta = randint(0, 1000)
            delta = -delta if not increasing else delta

            # check lights random

            if abs(delta) < 20:
                check_engine = 0 if check_engine else 1

            if abs(delta) > 980:
                check_oil = 0 if check_oil else 1

            if rpm > rpm_max_treshold and gear < 6:
                gear += 1
                rpm = rpm_min_treshold

            if rpm > rpm_max_treshold and gear == 6:
                increasing = False

            if rpm < rpm_min_treshold and gear > 1:
                gear -= 1
                rpm = rpm_max_treshold

            if rpm < rpm_min_treshold and gear == 1:
                increasing = True

            print rpm, gear, delta
            rpm += delta

            self.send_datagram(rpm, gear, check_engine, check_oil)

            sleep(0.25)

if __name__ == '__main__':
    reactor.listenUDP(8000, Server())
    reactor.run()
